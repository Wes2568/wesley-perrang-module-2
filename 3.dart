void main() {
  var app = App();

  print(app.appName);
  print(app.sector);
  print(app.developer);
  print(app.year);
  app.upper();
}

class App {
  String appName = "Amabani Africa";
  String sector = "Education Technology";
  String developer = "Mukundi Lambani";
  int year = 2021;

  upper() {
    print(appName.toUpperCase());
  }
}
